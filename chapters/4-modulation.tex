\section{Modulation}

\subsection{Termes techniques}
\paragraph{Porteuse} Fréquence/Sinusoïde centrale autour de laquelle la modulation sera effectuée.

\paragraph{Signal modulant} Signal d'entrée/d'information du système que l'on va moduler pour obtenir le signal à envoyer.

\paragraph{Signal modulé} Signal de sortie, qui ressemble à une sinusoïde mais avec quelques petites variations.

Il est le signal que l'on envoi réellement dans l'air (ou dans un câble).

\subsection{Pourquoi la modulation ?}
\paragraph{Utilité de la modulation} Si deux personnes transmettent chacun leurs informations sur leurs propres bandes, les deux signaux vont interférer.

\emph{Sans cette modulation}: Incapable de partager le canal avec un autre émetteur ou en wireless.

\paragraph{Comment moduler un signal ?} Il faut pour cela, transposer le signal autour d'une autre fréquence (porteuse).

La plupart du temps, la fréquence porteuse est beaucoup plus grande que la bande de fréquence  du signal (Par exemple, une fréquence en MHz et une bande en kHz pour la FM).

Cette bande de fréquence est liée aux variations du signal modulant.

Le signal modulé (après la modulation) ressemble très fort à une sinusoïde à la fréquence porteuse mais avec quelques petites variations.

Ce sont dans ces variations que ce trouve l'information du signal modulant (qui va moduler la sinusoïde)!!

+ on veut envoyer de l'information $\rightarrow$ + on devra faire varier le signal par rapport à la porteuse de départ $\rightarrow$ + la bande devra être large.


\subsection{Modulations analogiques}
\paragraph{Modulation en bande de base} Transmission sans modulation.\\
\emph{Analogique}: Envoyer directement le signal (Exemple: Signal venant du micro envoyé directement sur la ligne téléphonique)\\
\emph{Numérique}: Envoyer un signal carré (multi-niveaux).\\
Pour que ça fonctionne, il faut donc un canal où nous sommes seul à envoyer des données dessus (Exemple: téléphone, Ethernet, (ADSL))

\paragraph{Modulation d'amplitude}
\subparagraph{Première version}
Combinaison de l'amplitude du signal modulant et de la fréquence de la porteuse pour former un signal modulé sinusoïdal avec une amplitude qui varie.
\begin{figure}[H] \centering
    \includegraphics[width = 0.5\textwidth]{pictures/examples/modulation-amplitude1.png}
    \caption{Exemple de la $1^{ère}$ version de la modulation d'amplitude}
    \label{modulation-amplitude1}
\end{figure}
\emph{Problème}: L'amplitude ne doit pas descendre en dessous de 0.

Donc, pas possible de faire trop varier l'amplitude sinon apparition d'un effet de surmodulation.

\subparagraph{Deuxième version: Modulateur}
Version la plus fréquente de nos jours.

Multiplication du signal modulant avec une cosinusoïde (une certaine porteuse).
\begin{figure}[H] \centering
    \includegraphics[width = 0.5\textwidth]{pictures/examples/modulateur.png}
    \caption{Schéma d'un modulateur}
    \label{modulateur}
\end{figure}

Multiplier par une cosinusoïde à comme effet de décaler le spectre/le contenu fréquentiel dans les deux sens (sens positif et négatif) de la valeur du cosinus choisi.
\begin{figure}[H] \centering
    \includegraphics[width = 0.5\textwidth]{pictures/examples/modulation-amplitude2.png}
    \caption{Exemple de la $2^{ème}$ version de la modulation d'amplitude}
    \label{modulation-amplitude2}
\end{figure}

\paragraph{Filtrage} Permet de filtrer le signal résultant d'un modulateur pour garder uniquement les composantes intéressantes du contenu fréquentiel.
\begin{figure}[H] \centering
    \includegraphics[width = 0.7\textwidth]{pictures/examples/filtrage.png}
    \caption{Exemple de filtrage en terme de contenu fréquentiel}
    \label{filtrage-spectre}
\end{figure}
(La fréquence de coupure (noté $freq_{coup}$) correspond à la fréquence où le spectre va être séparer lors du filtrage)

\begin{itemize}
    \item \textbf{Filtre passe-bas (low-pass)}: Garde les composantes du contenu fréquentiel \textsc{en dessous} de la $freq_{coup}$ et retire celles \textsc{au dessus}.\\
    \item \textbf{Filtre passe-haut (high-pass)}: Garde les composantes du contenu fréquentiel \textsc{au dessus} de la $freq_{coup}$ et retire celles \textsc{en dessous}.\\
    \item \textbf{Filtre passe-bande (band-pass)}: Garde les composantes du contenu fréquentiel \textsc{entre} la $freq_{coup\_1}$ et la $freq_{coup\_2}$ (où $freq_{coup\_1} < freq_{coup\_2}$) et retire le reste.
\end{itemize}

\paragraph{Démodulateur} Le démodulateur multiplie par la même cosinusoïde qu'établit lors du passage dans le modulateur (Figure \ref{modulateur}).

Le résultat de cette multiplication va passer dans le filtre passe-bas pour récupérer uniquement les éléments intéressants autour du signal initial.
\begin{figure}[H] \centering
    \includegraphics[width = 0.5\textwidth]{pictures/examples/demodulateur.png}
    \caption{Schéma d'un démodulateur}
    \label{demodulateur}
\end{figure}

La fréquence de l'oscillateur local du démodulateur doit être la même que celle du modulateur (Figure \ref{modulateur})

\subparagraph{Démodulateur cohérent} Démodulateur ayant la même fréquence que la porteuse d'émission mais aussi la même phase.

Dû aux problèmes de synchronisation, il est plus facile d'obtenir un résultat avec une même fréquence (avoir des phases différentes n'influent pas autant que la fréquence sur la facilité d'avoir un résultat correct).

\paragraph{Bandes latérales} correspondent aux bandes formées à gauche et à droite d'un signal.

Ces deux bandes sont toujours symétriques !!\\
\begin{minipage}[t]{0.5\linewidth}
    \begin{figure}[H] \centering
        \includegraphics[width = 0.4\textwidth]{pictures/examples/bandes-laterales-in.png}
        \caption{Signal de départ composé de deux bandes latérales}
        \label{bandes-laterales-in}
    \end{figure}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth}
    \begin{figure}[H] \centering
        \includegraphics[width = 1\textwidth]{pictures/examples/bandes-laterales-out.png}
        \caption{Signal de départ modulé}
        \label{bandes-laterales-out}
    \end{figure}
\end{minipage}\\\\

Quand le signal de départ (Figure \ref{bandes-laterales-in}) est modulé (Figure \ref{bandes-laterales-out}), Celui-ci est translaté une fois vers la droite et une fois vers la gauche contenant chacun deux bandes:
\begin{itemize}
    \item La bande latérale supérieure (Upper sideband) [USB] qui est à droite,
    \item La bande latérale inférieure (Lower sideband) [LSB] qui est vers la gauche.
\end{itemize}
\emph{Rem}: Les bandes USB et LSB sont inversées dans la côté négatif du signal !!

\subsection{Bande latérale unique} Comme les deux bandes latérales sont toujours symétriques, il n'est pas nécessaire d'envoyer les deux bandes, une seule suffit.
\begin{figure}[H] \centering
    \includegraphics[width = 0.45\textwidth]{pictures/examples/example-bande-laterale-unique.png}
    \caption{Exemple de modulation d'une bande latérale d'un signal}
    \label{example-bande-laterale-unique}
\end{figure}

Il est aussi possible de facilement revenir au signal d'origine (grâce à la démodulation (Figure \ref{demodulateur})
\begin{figure}[H] \centering
    \includegraphics[width = 0.5\textwidth]{pictures/examples/example-bande-laterale-unique-demodulation.png}
    \caption{Exemple de démodulation d'un signal modulé en bande latérale unique (LSB)}
    \label{example-bande-laterale-unique-demodulation}
\end{figure}

Notons que dans la figure \ref{example-bande-laterale-unique-demodulation}, la démodulation reste identique à celle définie plus haut.

Remarquons l'intérêt de cette méthode car, grâce à cette technique, la démodulation n'utilise que la moitié de la bande passante qu'elle aurait du prendre en envoyant les deux bandes.

\subsection{Modulation numérique}
\emph{Grands avantages}:
\begin{itemize}
    \item Modulation analogique avec des signaux binaires
    \begin{itemize}
        \item Valeurs discrètes, càd un nombre limité de valeur
        \item Échantillons à cadence fixe, contrairement à la modulation analogique qui avait un signal continue en fonction du temps.
    \end{itemize}
    \item Permet de pouvoir régénérer le signal, il est plus facile de récupérer un signal avec du bruit.
\end{itemize}
\begin{figure}[H] \centering
    \includegraphics[width = 0.8\textwidth]{pictures/examples/types-modulation-num.png}
    \caption{Types de modulations numériques}
    \label{type-modulation-num}
\end{figure}

\begin{itemize}
    \item \textbf{ASK \textit{(Amplitude Shift Keying $\Rightarrow$ Variante de la modulation analogique AM)}}: L'\textsc{amplitude} varie en fonction de l'input 0 ou 1
    \item \textbf{FSK \textit{(Frequency Shift Keying $\Rightarrow$ Variante de la modulation analogique FM)}}: La \textsc{fréquence} varie en fonction de l'input 0 ou 1.
    \item \textbf{PSK \textit{(Phase Shift Keying)}}: La \textsc{phase} varie en fonction de l'input 0 ou 1.
\end{itemize}
Nous pouvons aussi définir la durée symbole, notée $T$, qui détermine la durée que va prendre un input (binaire) pour arriver.

\subsubsection{Mapping}
\paragraph{Constellation} Ensemble des points défini permettant d'attribuer des valeurs précises en fonction des valeurs binaires. Elle correspond à configuration du transfert d'information entre les bits et les symboles vers une antenne.\\
\emph{Exemple}: la constellation QAM (16-QAM (Figure \ref{16-qam}) est composée de 16 valeurs différentes), PSK (points en cercle), \dots
\paragraph{QAM (Quadrature Amplitude Modulation)} Envoi d'un élément sur une sinusoïde ($Q$) et un autre élément (pas forcément le même) sur une cosinusoïde ($I$).\\[0.5cm]
\begin{minipage}[t]{0.5\linewidth}
    \begin{figure}[H] \centering
        \includegraphics[width = 0.8\textwidth]{pictures/examples/16-qam.png}
        \caption{Exemple d'une modulation 16-QAM}
        \label{16-qam}
    \end{figure}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth}
    Exécution de la 16-QAM pour la série d'input suivante:
    \begin{enumerate}
        \item 00011010
        \item Découpage en 2 blocs de 4 bits : 0001 1010
        \item Exécution par série de 4 bits (1 byte)\\
    \end{enumerate}
    
    $1^{er}$ byte: 0001:
    \begin{itemize}
        \item[] $I(1) = 0.4472$
        \item[] $Q(1) = 1.3416$\\
    \end{itemize}
    
    $2^{ème}$ byte: 1010:
    \begin{itemize}
        \item[] $I(2) = -1.3416$
        \item[] $Q(2) = 0.4472$
    \end{itemize}
\end{minipage}\\

\paragraph{Fonctionnement du mapping}
\subparagraph{Envoi de données}
\begin{enumerate}
    \item L'on découpe les inputs en blocs.
    \item L'on crée un mapping: Choisir une constellation (et donc la création de deux séquences: $I$ et $Q$).
    \item Chacune de ces deux séquence (contenant les symboles extraits grâce au mapping vers les branches $I$ et $Q$) vont être mises en forme avec une certaine forme d'onde.
    \item Le résultat de ces mises en forme va donner des signaux carrés.
    \item Comme l'on veut éviter d'avoir des signaux carrés (car le changement trop rapide des données pose des problèmes au niveau du contenu fréquentiel), L'on multiplie les deux séquences, la branche $I$ par un cosinus et la branche $Q$ par un sinus.
    \item On additione finalement le résultat pour avoir ce que l'on va envoyer à l'antenne.
\end{enumerate}
\begin{figure}[H] \centering
    \includegraphics[width = 0.8\textwidth]{pictures/examples/mapping-sender.png}
    \caption{Schéma du mapping lors de l'envoi de données}
    \label{mapping-sender}
\end{figure}

\subparagraph{Réception de données}
\begin{enumerate}
    \item Lors de la réception, multiplication de la donnée par un sinus et par un cosinus.
    \item Application d'un filtre passe-bas pour enlever les données superflues créées lors de la multiplication
    \item Étape de décision permettant de récupérer les valeurs à chacun des instants
    \item Combination des résultats des étapes de décision pour réestimer les bits envoyés.
\end{enumerate}
\begin{figure}[H] \centering
    \includegraphics[width = 0.8\textwidth]{pictures/examples/mapping-receiver.png}
    \caption{Schéma du mapping lors de la réception de données}
    \label{mapping-receiver}
\end{figure}

\paragraph{Choix de la constellation}
\begin{itemize}
    \item Influe sur le débit $\Rightarrow$ Débit(16-QAM) > Débit(4-QAM)
    \item Influe sur la probabilité d'erreur  $\Rightarrow$ Risq\_ERR(16-QAM) > Risq\_ERR(4-QAM)
\end{itemize}

\subsubsection{Mise en forme}
\begin{figure}[H] \centering
    \includegraphics[width = 0.7\textwidth]{pictures/examples/spectre-mef-rect.png}
    \caption{Exemple d'un contenu fréquentiel d'une mise en forme rectangulaire [dB]}
    \label{spectre-mef-rect}
\end{figure}
\begin{itemize}
    \item Bcp de place en fréquence = trop grand contenu fréquentiel
    \item Pourquoi le contenu fréquentiel est trop grand ?
    \begin{itemize}
        \item[$\rightarrow$] Changement instantané de tension sur des signaux carrés
        \item[$\Rightarrow$] Changement très rapide
        \item[$\Rightarrow$] Contenu fréquentiel très élevé
    \end{itemize}
    \item \emph{Solution}: Changement plus doux grâce à une filtre de mise en forme
\end{itemize}
\begin{figure}[H] \centering
    \includegraphics[width = 0.9\textwidth]{pictures/examples/sign-carre-slower.png}
    \caption{Adoucissement d'un signal carré}
    \label{sign-carre-slower}
\end{figure}

\subsubsection{Décision}
Permet de retrouver les symboles à partir des échantillions bruités:
\begin{itemize}
    \item Choisir la valeur la plus proche dans la constellation (Étape de déduction de bits).
    \item Implique une probabilité d'erreur.
\end{itemize}

\begin{minipage}[t]{0.5\linewidth}
    \begin{figure}[H] \centering
        \includegraphics[width = 0.8\textwidth]{pictures/examples/decision-no-bruit.png}
        \caption{Constellation avec peu de bruit}
        \label{decision-no-bruit}
    \end{figure}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth}
    \begin{figure}[H] \centering
        \includegraphics[width = 0.8\textwidth]{pictures/examples/decision-bruit.png}
        \caption{Constellation avec beaucoup de bruit}
        \label{decision-bruit}
    \end{figure}
\end{minipage}\\

\newpage
